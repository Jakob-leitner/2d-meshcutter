using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class CutController : MonoBehaviour
{
    private LineRenderer lineRenderer;
    [SerializeField] private Camera cam;

    public List<CuttAbleMesh> cutableMeshList;

    private Vector3 dragStopPosition;
    private Vector3 dragStartPosition;


    void Awake()
    {
        
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
    }
    void Update()
    {
        HandleInteraction();
        UpdateShaderValues();
    }

    void HandleInteraction()
    {
        dragStopPosition = GetMouseWorldPosition();

        if (Input.GetMouseButtonDown(0))
        {
            lineRenderer.enabled = true;
            dragStartPosition = GetMouseWorldPosition();
            dragStopPosition = dragStartPosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            lineRenderer.enabled = false;
            foreach (var mesh in cutableMeshList)
            {
                var normal = Vector3.Cross(dragStopPosition - dragStartPosition, Vector3.forward).normalized;
                var lineThickness = lineRenderer.widthCurve.keys[0].value;
                mesh.halfMeshDivide(dragStartPosition - normal * lineThickness / 2f, dragStopPosition - normal * lineThickness / 2f);
            }
        }

        lineRenderer.SetPosition(0, dragStartPosition - Vector3.forward * 0.5f);
        lineRenderer.SetPosition(1, dragStopPosition - Vector3.forward * 0.5f);
    }

    private Vector3 GetMouseWorldPosition()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = cam.transform.position.magnitude;
        return cam.ScreenToWorldPoint(mousePosition);
    }

    private void UpdateShaderValues()
    {
        lineRenderer.sharedMaterial.SetVector("_DragStart", dragStartPosition);
        lineRenderer.sharedMaterial.SetVector("_DragStop", dragStopPosition);
    }

}
