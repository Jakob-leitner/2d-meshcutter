using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;
    private List<AudioSource> sources = new();

    [SerializeField] private int maxCount = 15;
    [SerializeField] private AudioClip chopSound;
    [SerializeField] private AudioClip dropSound;
    [SerializeField] private CutController cutController;


    private void Awake()
    {
        for (int i = 0; i < 10; i++)
        {
            sources.Add(gameObject.AddComponent<AudioSource>());
        }

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        cutController.cutableMeshList.ForEach((x)=>x.OnCut += PlayChopSound);
    }
   
    public void PlayChopSound()
    {
        var pitch = Random.Range(0.7f, 1.2f);
        var _source = GetSource();
        if (!_source) return;
        _source.clip = chopSound;
        _source.pitch = pitch;
        _source.volume = 1f;
        _source.Play();
    }

    public void PlayDropSound(float pitch)
    {
        var _source = GetSource();
        if (!_source) return;
        _source.clip = dropSound;
        _source.pitch = pitch;
        _source.volume = 0.5f;
        _source.Play();
    }

    AudioSource GetSource()
    {
        AudioSource _source = default;
        foreach (var source in sources)
        {
            if (!source.isPlaying)
            {
                _source = source;
                break;
            }
        }

        if (_source == null && sources.Count <= maxCount)
        {
            var newSource = gameObject.AddComponent<AudioSource>();
            _source = newSource;
            sources.Add(newSource);
            return _source;
        }

        return _source;
    }
}
