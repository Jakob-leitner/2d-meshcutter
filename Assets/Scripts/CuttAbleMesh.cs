using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Rendering;
using UnityEngine.U2D;
using static HalfEdgeDataStructure;
using Debug = UnityEngine.Debug;

public class CuttAbleMesh : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private Texture2D cutOffTexture;
    [SerializeField] private List<ushort> indices = new(3);
    [SerializeField] private List<Vector2> vertices = new(3);

    private Vector2 dragStartPosition;
    private Vector2 dragStopPosition;

    private Mesh mesh;
    private GameObject spriteGo;
    private Material cutOffMaterial;
    private MaterialPropertyBlock mpb;
    private PolygonCollider2D collider;
    private SpriteRenderer spriteRenderer;
    private HalfEdgeDataStructure halfEdgeMesh = new();

    public Action OnCut;

    //TODO: Support MeshRenderer And SpriteRenderer
    //TODO: Support Separated Geometry is also cuttable
    //TODO: Reconstuct Geometry according to its CornerCount
    //TODO: Remove MaterialOverrideCode
    //TODO: Handle that cuttable meshes are only beeing cut if DragStopPosition is not within any of these cuttable triangles
    private void Start()
    {
        if (!gameObject.GetComponent<PolygonCollider2D>())
            collider = gameObject.AddComponent<PolygonCollider2D>();
      
        GetMeshFromSprite();
        cutOffMaterial = spriteRenderer.sharedMaterial;

        SetupSpriteObject();
        ParseToHalfEdgeStructure(out halfEdgeMesh, vertices, indices);
        //Test feature
        {
            GetBoundrys(halfEdgeMesh);
        }
        ConstructSprite(spriteRenderer);
    }

    private void GetBoundrys(HalfEdgeDataStructure _halfEdgeMesh)
    {
        HalfEdge startBoundaryEdge = _halfEdgeMesh.edges.FirstOrDefault((x) => x.twin == null);
        Debug.Assert(startBoundaryEdge != null, "No Boundary found");
        List<HalfEdge> boundry = new List<HalfEdge>();
        HalfEdge current = startBoundaryEdge;

        do
        {
            boundry.Add(current);
            while (current.twin != null && boundry.Contains(current) == true)
            {
                current = current.prev.twin;
                if (current == null) current = current.twin;
            }
        } while (current != startBoundaryEdge);
    }

    private void GetMeshFromSprite()
    {
        mesh = new Mesh();
        spriteRenderer = GetComponent<SpriteRenderer>();
        vertices = spriteRenderer.sprite.vertices.ToList();
        indices = spriteRenderer.sprite.GetIndices().ToList();
    }

    public void halfMeshDivide(Vector3 D, Vector3 E)
    {
        dragStartPosition = D;
        dragStopPosition = E;

        var affectedFaces = SplitEdges(D, E, halfEdgeMesh.edges);
        if (affectedFaces == null) return;
        OnCut.Invoke();
        FillMesh(affectedFaces);
        Debug.Assert(halfEdgeMesh.subMesh.next != null, "SUBMESH IS NULL");
        var separatedFaces = FindContiguousHalfEdges(halfEdgeMesh.subMesh, halfEdgeMesh);
        RemoveFaces(halfEdgeMesh, separatedFaces);
        ParseToIndexedVertexBufferArray(halfEdgeMesh.faces, vertices, indices);
        ConstructSprite(spriteRenderer);

        //Create Cutoff Sprite
        var otherVerts = new List<Vector2>();
        var otherIndices = new List<ushort>();
        ParseToIndexedVertexBufferArray(separatedFaces, otherVerts, otherIndices);
        CreateNewSprite(otherVerts, otherIndices);
    }
    void FillMesh(List<CutFace> affectedFaces)
    {
        foreach (var face in affectedFaces)
        {
            var faceTop = new Face();
            var faceLeft = new Face();
            var faceRight = new Face();

            //Hier nochmal schauen ob das passt
            var heLeftTop = face.untouchedEdge.next.next;
            var heLeftBottom = face.untouchedEdge.next;
            var heBottom = face.untouchedEdge;
            var heRightTop = face.untouchedEdge.prev.prev;
            var heRightBottom = face.untouchedEdge.prev;

            //change the next 4 lines if you don't want the mesh to be cut into separate pieces
            //+ uncomment the set twin at the bottom
            var newVertex1 = new HalfEdgeDataStructure.Vertex(face.untouchedEdge.prev.vertex.position);
            var newVertex2 = new HalfEdgeDataStructure.Vertex(heLeftTop.vertex.position);

            halfEdgeMesh.vertices.Add(newVertex1);
            halfEdgeMesh.vertices.Add(newVertex2);

            var newHalfEdgeAbove = new HalfEdge(newVertex1, faceTop);
            var newHalfEdgeBelow = new HalfEdge(newVertex2, faceRight);
            var newHalfEdgeToUntouchedDown = new HalfEdge(heLeftTop.vertex, faceLeft);
            var newHalfEdgeToUntouchedUp = new HalfEdge(heBottom.vertex, faceRight);


            //set new faces
            faceTop.HalfEdge = heLeftTop;
            faceLeft.HalfEdge = heLeftBottom;
            faceRight.HalfEdge = heRightBottom;

            //set vertices halfedges here
            //
            //

            //set upper triangle
            heRightTop.next = newHalfEdgeAbove;
            heLeftTop.prev = newHalfEdgeAbove;
            newHalfEdgeAbove.prev = heRightTop;
            newHalfEdgeAbove.next = heLeftTop;

            //set right triangle
            heRightBottom.prev = newHalfEdgeBelow;
            heRightBottom.next = newHalfEdgeToUntouchedUp;
            newHalfEdgeToUntouchedUp.prev = heRightBottom;
            newHalfEdgeToUntouchedUp.next = newHalfEdgeBelow;
            newHalfEdgeBelow.prev = newHalfEdgeToUntouchedUp;
            newHalfEdgeBelow.next = heRightBottom;

            //set left triangle
            heLeftBottom.next = newHalfEdgeToUntouchedDown;
            heLeftBottom.prev = heBottom;
            heBottom.prev = newHalfEdgeToUntouchedDown;
            newHalfEdgeToUntouchedDown.next = heBottom;
            newHalfEdgeToUntouchedDown.prev = heLeftBottom;

            //set twins
            newHalfEdgeToUntouchedUp.twin = newHalfEdgeToUntouchedDown;
            newHalfEdgeToUntouchedDown.twin = newHalfEdgeToUntouchedUp;
            //newHalfEdgeAbove.twin = newHalfEdgeBelow;
            //newHalfEdgeBelow.twin = newHalfEdgeAbove;

            //setfaces
            newHalfEdgeAbove.face = faceTop;
            newHalfEdgeBelow.face = faceRight;
            newHalfEdgeToUntouchedUp.face = faceRight;
            newHalfEdgeToUntouchedDown.face = faceLeft;
            heLeftBottom.face = faceLeft;
            heLeftTop.face = faceTop;
            heBottom.face = faceLeft;
            heRightBottom.face = faceRight;
            heRightTop.face = faceTop;


            //addfaces and edges to lists
            halfEdgeMesh.faces.Add(faceTop);
            halfEdgeMesh.faces.Add(faceLeft);
            halfEdgeMesh.faces.Add(faceRight);

            //remove old face
            halfEdgeMesh.faces.Remove(face.face);

            halfEdgeMesh.edges.Add(newHalfEdgeBelow);
            halfEdgeMesh.edges.Add(newHalfEdgeAbove);
            halfEdgeMesh.edges.Add(newHalfEdgeToUntouchedUp);
            halfEdgeMesh.edges.Add(newHalfEdgeToUntouchedDown);
        }

        foreach (var edge in halfEdgeMesh.edges)
        {
            if (halfEdgeMesh.subMesh == null) SetSubMesh(edge);
        }
    }

    private List<CutFace> SplitEdges(Vector3 D, Vector3 E, List<HalfEdge> edges)
    {
        halfEdgeMesh.subMesh = null;
        List<CutFace> affectedFaces = new();
        HashSet<HalfEdge> dontConsiderEdges = new();
        var edgesCopy = new List<HalfEdge>(edges.ToArray());
        var intersectedBoundryEdges = new List<HalfEdge>();


        //this Dictionary keeps track of what of the 3 adjacent edges is the untouched one ([0])
        Dictionary<Face, List<HalfEdge>> cutFacesDictionary = new();
        foreach (var face in halfEdgeMesh.faces)
        {
            cutFacesDictionary.Add(face, new List<HalfEdge>());
            cutFacesDictionary[face].Add(face.HalfEdge.prev);
            cutFacesDictionary[face].Add(face.HalfEdge);
            cutFacesDictionary[face].Add(face.HalfEdge.next);
        }

        foreach (var edge in edgesCopy)
        {

            if (dontConsiderEdges.Contains(edge)) continue;
            Debug.Assert(edge.face != null, "SHOULD NOT HAPPEN");

            Vector2 A = edge.vertex.position;
            Vector2 B = edge.next.vertex.position;
            Vector3 AB = localToWorld(B) - localToWorld(A);

            bool isIntersecting = Extensions.IntersectionPointWithinLength(localToWorld(A), D, AB,
                E - D, out Vector2 intersectionPoint);

            if (!isIntersecting) continue;

            if (edge.twin == null)
                intersectedBoundryEdges.Add(edge);

            //remove from untouchedEdges
            cutFacesDictionary[edge.face].Remove(edge);
            if (edge.twin != null)
                cutFacesDictionary[edge.twin.face].Remove(edge.twin);

            //split edge
            var intersectionVertex = new Vertex(worldToLocal(intersectionPoint));
            var newEdge = new HalfEdge(intersectionVertex, edge.face);
            newEdge.prev = edge;
            newEdge.next = edge.next;
            newEdge.next.prev = newEdge;
            edge.next = newEdge;
            halfEdgeMesh.edges.Add(newEdge);

            dontConsiderEdges.Add(edge);
            //handle border edges
            if (edge.twin != null)
            {
                //split edge's twin
                var edgeOpposite = edge.twin;
                var newEdgeOpposite = new HalfEdge(intersectionVertex, edge.twin.face);// hier zu edge.twin.face statt edge.face ge�ndert
                newEdgeOpposite.prev = edgeOpposite;
                newEdgeOpposite.next = edgeOpposite.next;
                newEdgeOpposite.next.prev = newEdgeOpposite;
                edgeOpposite.next = newEdgeOpposite;
                dontConsiderEdges.Add(edgeOpposite);
                halfEdgeMesh.edges.Add(newEdgeOpposite);

                //set twins
                newEdge.twin = edgeOpposite;
                newEdgeOpposite.twin = edge;
                edge.twin = newEdgeOpposite;
                edgeOpposite.twin = newEdge;

                //set opposite face
                newEdgeOpposite.face = edgeOpposite.face;

                //add opposite edge to list
                if (affectedFaces.Count(x => x.face == edgeOpposite.face) == 0)
                    affectedFaces.Add(new CutFace(edgeOpposite.face, null));
            }


            //set faces
            newEdge.face = edge.face;

            //add vertex
            halfEdgeMesh.vertices.Add(intersectionVertex);

            //add edge to list
            if (affectedFaces.Count(x => x.face == edge.face) == 0)
                affectedFaces.Add(new CutFace(edge.face, null));
        }
        Debug.Assert(intersectedBoundryEdges.Count <= 2, $"Intersected Boundry Edges-Counter: {intersectedBoundryEdges.Count}.");
        if (intersectedBoundryEdges.Count < 2)//???
            return null;

        foreach (var affectedFace in affectedFaces)
        {
            Debug.Assert(cutFacesDictionary[affectedFace.face].Count == 1, $" {cutFacesDictionary[affectedFace.face].Count} UNTOUCHED EDGES");
            affectedFace.untouchedEdge = cutFacesDictionary[affectedFace.face][0];
        }

        return affectedFaces;
    }
    private void SetSubMesh(HalfEdge _edge)
    {
        var avg = _edge.vertex.position + _edge.prev.vertex.position + _edge.next.vertex.position;
        avg /= 3;
        avg = localToWorld(avg);
        var dragVector = dragStopPosition - dragStartPosition;
        var normal = Vector3.Cross(dragVector.normalized, Vector3.forward);
        var dotProduct = Vector2.Dot(normal, (avg - dragStartPosition).normalized);

        if (dotProduct > 0)
            halfEdgeMesh.subMesh = _edge;
    }
    private void CreateNewSprite(List<Vector2> _vertices, List<ushort> _indices)
    {
        var go = Instantiate(spriteGo, transform);
        go.transform.SetParent(null, true);
        var spriteRenderer = go.GetComponent<SpriteRenderer>();
        spriteRenderer.SetPropertyBlock(mpb);

       
        Sprite newSprite = Instantiate(this.spriteRenderer.sprite);


        var verticesCopy = new List<Vector2>(_vertices.ToArray());
        var worldSpaceVertices = new List<Vector2>(verticesCopy);

        var uvVertices = new NativeArray<Vector2>(verticesCopy.Count, Allocator.Temp);
        var pos = new NativeArray<Vector3>(verticesCopy.Count, Allocator.Temp);
        var ind = new NativeArray<ushort>(_indices.Count, Allocator.Temp);
        for (int i = 0; i < worldSpaceVertices.Count; i++)
        {
            uvVertices[i] = (worldSpaceVertices[i] / 2f) * 0.5f;
            pos[i] = worldSpaceVertices[i];
        }

        for (int i = 0; i < _indices.Count; i++)
        {
            ind[i] = _indices[i];
        }

        newSprite.SetVertexCount(verticesCopy.Count);
        newSprite.SetVertexAttribute(VertexAttribute.Position, pos);
        newSprite.SetIndices(ind);

        spriteRenderer.sprite = newSprite;

        //create custom sprite collision geometry
        CreateSpriteCollider(go, _indices, worldSpaceVertices);

        go.GetComponent<Rigidbody2D>().simulated = true;
        spriteRenderer.sharedMaterial = cutOffMaterial;
    }

    private static void CreateSpriteCollider(GameObject go, List<ushort> _indices, List<Vector2> _vertices)
    {
        var collider = go.GetComponent<PolygonCollider2D>();
        if (!collider)
            collider = go.AddComponent<PolygonCollider2D>();


        collider.pathCount = _indices.Count / 3;
        collider.enabled = false;
        //A Better way to find the collisionshape is to get the edges in the right order, then connect them like this 10 02 23 31 this would be the outline then
        int counter = 0;
        for (int i = 0; i < _indices.Count; i += 3)
        {
            collider.SetPath(counter,
                new Vector2[]
                {
                    _vertices[_indices[i]], _vertices[_indices[i + 1]], _vertices[_indices[i + 2]]
                });
            counter++;
        }
        collider.enabled = true;
    }
    private void ConstructSprite(SpriteRenderer _spriteRenderer)
    {
        CreateSpriteCollider(this.gameObject, indices, vertices);

        var uvVertices = new NativeArray<Vector2>(vertices.Count, Allocator.Temp);
        var pos = new NativeArray<Vector3>(vertices.Count, Allocator.Temp);
        var ind = new NativeArray<ushort>(indices.Count, Allocator.Temp);
        for (int i = 0; i < vertices.Count; i++)
        {
            uvVertices[i] = (vertices[i] / 2f) * 0.5f;
            pos[i] = vertices[i];
        }

        for (int i = 0; i < indices.Count; i++)
        {
            ind[i] = indices[i];
        }

        Sprite newSprite = Instantiate(_spriteRenderer.sprite);
        newSprite.SetVertexCount(vertices.Count);
        newSprite.SetVertexAttribute(VertexAttribute.Position, pos);
        newSprite.SetIndices(ind);

        _spriteRenderer.sprite = newSprite;
    }
    private void SetupSpriteObject()
    {
        spriteGo = new GameObject("CutOff_Sprite");
        spriteGo.AddComponent<SoundOnCollision>();
        var rb = spriteGo.AddComponent<Rigidbody2D>();
        spriteGo.AddComponent<SpriteRenderer>();

        rb.simulated = false;
        rb.bodyType = RigidbodyType2D.Dynamic;
        rb.useAutoMass = true;

        var collider = spriteGo.AddComponent<PolygonCollider2D>();
        collider.sharedMaterial = GetComponent<Rigidbody2D>().sharedMaterial;
    }

    private Vector2 localToWorld(Vector2 pos)
    {
        var convertedPosition = transform.localToWorldMatrix * new Vector4(pos.x, pos.y, 0, 1);
        return new Vector2(convertedPosition.x, convertedPosition.y);
    }

    private Vector2 worldToLocal(Vector2 pos)
    {
        var convertedPosition = transform.worldToLocalMatrix * new Vector4(pos.x, pos.y, 0, 1);
        return new Vector2(convertedPosition.x, convertedPosition.y);
    }




    #region helper
    void TestFaceEdgeCount(List<CutFace> affectedFaces)
    {
        foreach (var face in affectedFaces)
        {
            var buffer = face.face.HalfEdge;
            var counter = 0;
            do
            {
                counter++;
                buffer = buffer.next;
            } while (buffer != face.face.HalfEdge);
            Debug.Assert(counter == 5, "SHOULD NOT HAPPEN");
        }
    }

    void OnDrawGizmos()
    {
    }
    #endregion

    struct CustomVertex
    {
        public Vector3 position;
        public Vector2 uv;
    }
}
