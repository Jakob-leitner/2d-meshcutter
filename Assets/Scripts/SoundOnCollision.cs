using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SoundOnCollision : MonoBehaviour
{
    private Rigidbody2D rb;
    private float timeSinceLastCollision = 0.5f;
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        timeSinceLastCollision += Time.deltaTime;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (rb.velocity.magnitude >= 1f && timeSinceLastCollision >= 0.5f)
        {
            //pitch is proportional to mass
            float pitch = Remap(rb.mass, 0, 2.3f, 0.1f, 0.8f);
            pitch = 0.9f - pitch;

            AudioManager.Instance.PlayDropSound(pitch);
            timeSinceLastCollision = 0;
        }
    }

    public static float Remap(float value, float fromMin, float fromMax, float toMin, float toMax)
    {
        float normalizedValue = Mathf.InverseLerp(fromMin, fromMax, value);
        return Mathf.Lerp(toMin, toMax, normalizedValue);
    }
}
