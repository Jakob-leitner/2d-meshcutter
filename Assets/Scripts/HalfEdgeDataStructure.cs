using System;
using System.Collections.Generic;
using UnityEngine;
using static HalfEdgeDataStructure;

public class HalfEdgeDataStructure : MonoBehaviour
{
    public List<Vertex> vertices;
    public List<HalfEdge> edges;
    public List<Face> faces;
    public List<CutFace> cutFaces;
    public Dictionary<Vector2, HalfEdge> edgesDict;
    public HalfEdge subMesh;
    
    public HalfEdgeDataStructure()
    {
        vertices = new List<Vertex>();
        edges = new List<HalfEdge>();
        faces = new List<Face>();
        cutFaces = new List<CutFace>();
        edgesDict = new Dictionary<Vector2, HalfEdge>();
    }

    public static void ParseToHalfEdgeStructure(out HalfEdgeDataStructure halfEdgeMesh, List<Vector2> vertices, List<ushort> indices)
    {
        halfEdgeMesh = new HalfEdgeDataStructure();
        Dictionary<string, HalfEdge> halfEdgeDict = new();

        for (int i = 0; i < indices.Count; i += 3)
        {
            var face = new Face();

            var vertex1 = new Vertex(vertices[indices[i]]);
            var vertex2 = new Vertex(vertices[indices[i + 1]]);
            var vertex3 = new Vertex(vertices[indices[i + 2]]);

            var halfEdge1 = new HalfEdge(vertex1, face);
            var halfEdge2 = new HalfEdge(vertex2, face);
            var halfEdge3 = new HalfEdge(vertex3, face);
            face.HalfEdge = halfEdge1;

            vertex1.HalfEdge = halfEdge1;
            vertex2.HalfEdge = halfEdge2;
            vertex3.HalfEdge = halfEdge3;

            string start1 = Math.Min(indices[i], indices[i + 1]).ToString();
            string end1 = Math.Max(indices[i], indices[i + 1]).ToString();
            string key1 = start1 + "_" + end1;

            string start2 = Math.Min(indices[i + 1], indices[i + 2]).ToString();
            string end2 = Math.Max(indices[i + 1], indices[i + 2]).ToString();
            string key2 = start2 + "_" + end2;

            string start3 = Math.Min(indices[i], indices[i + 2]).ToString();
            string end3 = Math.Max(indices[i], indices[i + 2]).ToString();
            string key3 = start3 + "_" + end3;

            halfEdge1.prev = halfEdge3;
            halfEdge1.next = halfEdge2;
            halfEdge1.face = face;
            halfEdge1.twin = null;
            halfEdge1.vertex = vertex1;

            halfEdge2.prev = halfEdge1;
            halfEdge2.next = halfEdge3;
            halfEdge2.face = face;
            halfEdge2.twin = null;
            halfEdge2.vertex = vertex2;

            halfEdge3.prev = halfEdge2;
            halfEdge3.next = halfEdge1;
            halfEdge3.face = face;
            halfEdge3.twin = null;
            halfEdge3.vertex = vertex3;

            if (halfEdgeDict.ContainsKey(key1)) //Is Edge1 a "DoubleEdge"?
            {
                var foundEdge = halfEdgeDict[key1];
                foundEdge.twin = halfEdge1;
                halfEdge1.twin = foundEdge;
            }
            else
            {
                halfEdgeDict[key1] = halfEdge1;
            }

            if (halfEdgeDict.ContainsKey(key2)) //Is Edge2 a "DoubleEdge"?
            {
                var foundEdge = halfEdgeDict[key2];
                foundEdge.twin = halfEdge2;
                halfEdge2.twin = foundEdge;
            }
            else
            {
                halfEdgeDict[key2] = halfEdge2;
            }

            if (halfEdgeDict.ContainsKey(key3)) //Is Edge3 a "DoubleEdge"?
            {
                var foundEdge = halfEdgeDict[key3];
                foundEdge.twin = halfEdge3;
                halfEdge3.twin = foundEdge;
            }
            else
            {
                halfEdgeDict[key3] = halfEdge3;
            }

            halfEdgeMesh.faces.Add(face);

            halfEdgeMesh.vertices.Add(vertex1);
            halfEdgeMesh.vertices.Add(vertex2);
            halfEdgeMesh.vertices.Add(vertex3);

            halfEdgeMesh.edges.Add(halfEdge1);
            halfEdgeMesh.edges.Add(halfEdge2);
            halfEdgeMesh.edges.Add(halfEdge3);
        }
    }
    public static void ParseToIndexedVertexBufferArray( List<Face> faces, List<Vector2> _vertices, List<ushort> _indices)
    {
        _indices.Clear();
        _vertices.Clear();
        Dictionary<Vector3, ushort> uniqueVertices = new Dictionary<Vector3, ushort>();
        ushort counter = 0;

        for (ushort i = 0; i < faces.Count; i++)
        {
            var vertex0 = faces[i].HalfEdge.prev.vertex.position;
            var vertex1 = faces[i].HalfEdge.vertex.position;
            var vertex2 = faces[i].HalfEdge.next.vertex.position;

            if (!uniqueVertices.ContainsKey(vertex0))
            {
                _vertices.Add(vertex0);
                uniqueVertices[vertex0] = counter;
                _indices.Add(counter);
                counter++;
            }
            else
                _indices.Add(uniqueVertices[vertex0]);
       

            if (!uniqueVertices.ContainsKey(vertex1))
            {
                _vertices.Add(vertex1);
                uniqueVertices[vertex1] = counter;
                _indices.Add(counter);
                counter++;
            }
            else
                _indices.Add(uniqueVertices[vertex1]);

            if (!uniqueVertices.ContainsKey(vertex2))
            {
                _vertices.Add(vertex2);
                uniqueVertices[vertex2] = counter;
                _indices.Add(counter);
                counter++;
            }
            else
                _indices.Add(uniqueVertices[vertex2]);

        }
    }

    public class Vertex
    {
        public Vector2 position;
        public HalfEdge HalfEdge;

        public Vertex(Vector2 pos)
        {
            position = pos;
            HalfEdge = null;
        }
    }

    public class Face
    {
        public HalfEdge HalfEdge;

        public Face()
        {
            HalfEdge = null;
        }
    }

    public class CutFace
    {
        public HalfEdge untouchedEdge;
        public Face face;
        public CutFace(Face face, HalfEdge untouchedEdge)
        {
            this.face = face;
            this.untouchedEdge = untouchedEdge;
        }
    }

    public class HalfEdge
    {
        public Vertex vertex;
        public Face face;
        public HalfEdge next;
        public HalfEdge prev;
        public HalfEdge twin;

        public HalfEdge(Vertex v, Face f)
        {
            vertex = v;
            face = f;
            next = null;
            prev = null;
            twin = null;
        }
    }

    public static List<Face> FindContiguousHalfEdges(HalfEdge startHalfEdge, HalfEdgeDataStructure halfEdgeMesh)
    {
        Queue<HalfEdge> queue = new Queue<HalfEdge>();
        HashSet<HalfEdge> visited = new HashSet<HalfEdge>();
        List<Face> continousFaces = new List<Face>();

        // Enqueue the starting halfedge
        queue.Enqueue(startHalfEdge);
        visited.Add(startHalfEdge);

        while (queue.Count > 0)
        {
            HalfEdge currentHalfEdge = queue.Dequeue();
            if(!continousFaces.Contains(currentHalfEdge.face))
                 continousFaces.Add(currentHalfEdge.face);
            Debug.Assert(currentHalfEdge != null, "was");
            // Add the next, prev, and opposite halfedges to the queue if they haven't been visited

            //halfEdgeMesh.faces.Remove(currentHalfEdge.face);
            //halfEdgeMesh.edges.Remove(face.HalfEdge.prev);
            //halfEdgeMesh.edges.Remove(face.HalfEdge);
            //halfEdgeMesh.edges.Remove(face.HalfEdge.next);

            if (!visited.Contains(currentHalfEdge.next))
            {
                queue.Enqueue(currentHalfEdge.next);
                visited.Add(currentHalfEdge.next);
            }
            if (!visited.Contains(currentHalfEdge.prev))
            {
                queue.Enqueue(currentHalfEdge.prev);
                visited.Add(currentHalfEdge.prev);
            }
            if (currentHalfEdge.twin != null && !visited.Contains(currentHalfEdge.twin))
            {
                queue.Enqueue(currentHalfEdge.twin);
                visited.Add(currentHalfEdge.twin);
            }
        }

        return continousFaces;
    }

    //maybe do that inside the findContinuous Function
    public static void RemoveFaces(HalfEdgeDataStructure halfEdgeMesh, List<Face> facesToRemove)
    {
        foreach (var face in facesToRemove)
        {
            //do also edge the edges to "separatedmesh".edges
            halfEdgeMesh.faces.Remove(face);
            halfEdgeMesh.edges.Remove(face.HalfEdge.prev);
            halfEdgeMesh.edges.Remove(face.HalfEdge);
            halfEdgeMesh.edges.Remove(face.HalfEdge.next);
        }
    }

}
















