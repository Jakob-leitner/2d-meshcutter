using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{

    public static Vector3[] toVector3Array(List<Vector2> v2)
    {
        var v2A = v2.ToArray();
        return System.Array.ConvertAll<Vector2, Vector3>(v2A, getV3fromV2);
    }

    public static Vector3 getV3fromV2(Vector2 v2)
    {
        return new Vector3(v2.x, v2.y, 0f);
    }

    public static Vector2[] toVector2Array(List<Vector3> v3)
    {
        var v3A = v3.ToArray();
        return System.Array.ConvertAll<Vector3, Vector2>(v3A, getV2fromV3);
    }

    public static Vector2 getV2fromV3(Vector3 v3)
    {
        return new Vector2(v3.x, v3.y);
    }

    public static Vector2 clampVector2(Vector2 v2, Vector2 min, Vector2 max)
    {
        if(v2.x >= max.x) v2.x = max.x;
        if(v2.y >= max.y) v2.y = max.y;
        if(v2.x <= min.x) v2.x = min.x;
        if(v2.y <= min.y) v2.y = min.y;

        return v2;
    }

    public static float Remap(this float source, float sourceFrom, float sourceTo, float targetFrom, float targetTo)
    {
        return targetFrom + (source - sourceFrom) * (targetTo - targetFrom) / (sourceTo - sourceFrom);
    }
    public static Vector2 worldToLocal(Vector2 pos, Transform transform) //move to Extensions
    {
        return transform.worldToLocalMatrix * new Vector4(pos.x, pos.y, 0, 1);
    }

    public static Vector2 localToWorld(Vector2 pos, Transform transform) //move to Extensions
    {
        return transform.localToWorldMatrix * new Vector4(pos.x, pos.y, 0, 1);
    }

    public static bool IntersectionPointWithinLength(Vector2 position1, Vector2 position2, Vector2 vector1, Vector2 vector2, out Vector2 intersectionPoint)
    {
        float cross = vector1.x * vector2.y - vector1.y * vector2.x;

        if (Mathf.Approximately(cross, 0f))
        {
            intersectionPoint = Vector2.zero; // Parallel lines, no intersection
            return false;
        }

        Vector2 line = position2 - position1;
        float t = (line.x * vector2.y - line.y * vector2.x) / cross;
        float u = (line.x * vector1.y - line.y * vector1.x) / cross;

        if (t < 0f || t > 1f || u < 0f || u > 1f)
        {
            intersectionPoint = Vector2.zero; // Intersection point is outside the bounds of the input vectors
            return false;
        }

        intersectionPoint = position1 + t * vector1;
        return true;
    }
}
